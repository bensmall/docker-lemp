#/bin/sh

user=$(id -u)
group=$(id -g)

sed -i "s|USER_ID=[0-9]*|USER_ID=$user|g" ./.env
sed -i "s|GROUP_ID=[0-9]*|GROUP_ID=$group|g" ./.env